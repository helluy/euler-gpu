#define _LX _Lx_
#define _LY _Ly_
#define _NX _nx_
#define _NY _ny_
#define _DX _dx_
#define _DY _dy_
#define _DT _dt_
#define _M _m_

#define _LAMBDA _lambda_

#define _G (1.4_F)
#define _CV (720.0_F)

#define _VOL (_DX * _DY)

#define SCHEMA_FUX 1 // ==0 : Rusanov (choix par defaut), ==1 : Relaxation CC

__constant int dir[4][2] = { {1, 0}, {-1, 0},
			     {0, 1}, {0, -1}};

__constant float ds[4] = { _DY, _DY, _DX, _DX };

//#define double float

///////////////////////////////////////////////////////////////////////
// fonctions thermodynamiques Gaz Parfaits

float celer_re(float rho, float e){
return sqrt(_G*(_G-1.0_F)*e);
}

float press_re(float rho, float e){
return (_G-1.0_F)*rho*e;
}

float temp_re(float rho, float e){
return e/_CV;
}

float rho_pt(float P, float T){
return P/(_G-1.0_F)/(_CV*T);
}

float energ_pt(float P, float T){
return _CV*T;
}


///////////////////////////////////////////////////////////////////////
// fonction minmod
float minmod(float X, float Y)
{
float res=0.0_F;
if (X>=0.0_F & Y>=0.0_F) return fmin(X,Y);
if (X<=0.0_F & Y<=0.0_F) return fmax(X,Y);
return res;
}

///////////////////////////////////////////////////////////////////////
void fluxphy(float *w, float* n, float *flux){

  float rho = w[0];
  float u = w[1] / rho;
  float v = w[2] / rho;
  float un = u * n[0] + v * n[1];
  float e=w[3]/rho-0.5_F*(u*u+v*v);
  float P = press_re(rho,e);

  flux[0] = rho * un;
  flux[1] = w[1] * un + P * n[0];  
  flux[2] = w[2] * un + P * n[1];  
  flux[3] = (w[3] + P ) * un;  
  flux[4] = w[4] * un;
}

///////////////////////////////////////////////////////////////////////
void fluxnum(float *wL, float *wR, float* vnorm, float* flux, float* rspec){
  float uL= ( wL[1] * vnorm[0] + wL[2] * vnorm[1] ) / wL[0];
  float vL= ( wL[1] * vnorm[1] + wL[2] * (-vnorm[0]) ) / wL[0];
  float uR= ( wR[1] * vnorm[0] + wR[2] * vnorm[1] ) / wR[0];
  float vR= ( wR[1] * vnorm[1] + wR[2] * (-vnorm[0]) ) / wR[0];
  float eL=wL[3]/ wL[0]-0.5_F*(uL*uL+vL*vL);
  float eR=wR[3]/ wR[0]-0.5_F*(uR*uR+vR*vR);
  float tauL=1.0_F / wL[0];
  float tauR=1.0_F / wR[0];
  float alphaL=wL[4]*tauL;
  float alphaR=wR[4]*tauR;
  float cL=celer_re(1.0_F/tauL,eL); 
  float cR=celer_re(1.0_F/tauR,eR); 
  float rayon_spec=fmax(fabs(uL)+cR,fabs(uL)+cL);

  *rspec=rayon_spec;

  //--------------------------------------------------------
  // Flux de Relaxation Chalons-Coulombelle
  //--------------------------------------------------------
  if (SCHEMA_FUX==1){
    // var de relaxation L
    float PiL=press_re(1.0_F/tauL,eL); 
    float SigmaL=wL[3] / wL[0];
    float GTL=tauL;
    // var de relaxation R
    float PiR=press_re(1.0_F/tauR,eR);
    float SigmaR=wR[3] / wR[0];
    float GTR=tauR;
    // cond stab
    float a=1.00001_F*fmax(fabs(cL)/tauL, fabs(cR)/tauR);
    // etat intermediaires / sol
    float uSt=0.5_F*(uL+uR)+0.5_F*(PiL-PiR)/a;   
    float PiSt=0.5_F*a*(uL-uR)+0.5_F*(PiL+PiR);
    float tau,u,v,Pi,GT,Sigma,ux,uy,alpha;
     // vps
    float lambda1=uL-a*tauL;
    float lambda2=uSt;
    float lambda3=uR+a*tauR;
    *rspec=fmax(lambda1,lambda3);

   // calcul de l'etat en x/t=0
    if (lambda1>0.0_F){
      alpha=alphaL;
      tau=tauL;
      u=uL;
      v=vL;
      Pi=PiL;
      GT=GTL;
      Sigma=SigmaL;
      }
    else if (lambda2>0.0_F){
      alpha=alphaL;
      tau=tauL+(uSt-uL)/a;
      u=uSt;
      v=vL;  
      GT=GTL;
      Pi=PiSt;
      Sigma=SigmaL+(PiL*uL-PiSt*uSt)/a;
      }
    else if (lambda3>0.0_F){
      alpha=alphaR;
      tau=tauR-(uSt-uR)/a;
      u=uSt;
      v=vR;
      GT=GTR;
      Pi=PiSt;
      Sigma=SigmaR-(PiR*uR-PiSt*uSt)/a;
      }
    else{
      alpha=alphaR;
      tau=tauR;
      u=uR;
      v=vR;
      GT=GTR;
      Pi=PiR;
      Sigma=SigmaR;
      } // fin if PBR
   // reprojection de u,v qui sont projettes sur normal vnorm
   ux= (  u * vnorm[0] + v * vnorm[1] ) ;
   uy= (  u * vnorm[1] - v * vnorm[0] ) ;
   // flux
   flux[0]= u/tau;
   flux[1]=u*ux/tau+Pi*vnorm[0];
   flux[2]=u*uy/tau+Pi*vnorm[1];
   flux[3]=u*(Sigma/tau+Pi);
   flux[4]= alpha*u/tau;
   } // fin Relax CC
  //--------------------------------------------------------
  // Flux de Rusanov (choix par par defaut)
  //--------------------------------------------------------
  else { 
    float fL[_M];
    float fR[_M];
    fluxphy(wL, vnorm, fL);
    fluxphy(wR, vnorm, fR);

    for(int iv = 0; iv < _M; iv++){
      flux[iv] = 0.5_F * (fL[iv] + fR[iv]) - 0.5_F * rayon_spec * (wR[iv] - wL[iv]);   
    } // fin Rusanov

}
  
  //printf("%e %e %e %e\n",flux[0],flux[1],flux[2],flux[3]);

}


///////////////////////////////////////////////////////////////////////
void exact_sol(float* xy, float t, float* w){
  float alpha,Temp,rho,P,u,v;
  float x = xy[0];// - 0.5_F;
  float y = xy[1];// - 0.5_F;

 
//--------------------------------------------------------------------------------------------------------//
// obstable rectangulaire
//--------------------------------------------------------------------------------------------------------//
  alpha=0.0_F;
  Temp=293.0_F;
  P = 100000.0_F;
  if ( (x-1.0f)*(x-1.0f)+(y-1.0f)*(y-1.0f) < 0.4f){
    alpha=1.0_F;
    Temp=293.0_F;
    P = 800000.0_F;
  }
  u = 0.0_F;
  v = 0.0_F;
//--------------------------------------------------------------------------------------------------------//
// PBR
//--------------------------------------------------------------------------------------------------------//
/*  alpha=1.0_F;
  Temp=293.0_F;
  P = 800000.0_F;
  if ( x>0.5_F ){
    alpha=0.0_F;
    Temp=293.0_F;
    P = 100000.0_F;
  }
  u = 0.0_F;
  v = 0.0_F;
*/

//--------------------------------------------------------------------------------------------------------//
  rho = rho_pt(P,Temp);
  w[0] = rho;
  w[1] = rho*u;
  w[2] = rho*v;
  w[3] = rho*energ_pt(P,Temp)+0.5_F*rho*(u*u+v*v);
  w[4] = rho*alpha;

 // printf("%e\n",w[3]);

}

///////////////////////////////////////////////////////////////////////
// gestion des CL
// retourne 1000*x1+100*x2+10*x3+1*x4
// ou xi correspond a la couleur de la face i, avec i={east,west,north,south}
// si xi=0 : face interne
// si xi=1 : face paroi
// si xi=2 : face sortie
// ...
// Donc : si is_internal_cell() retourne 0 on a une cellule interne, 
//        si retourne -1 cellule externe a ne pas calculer,
//        sinon c'est une cellule de bord.
int is_internal_cell(int i, int j){
int res=0;
float xy[2] = {i * _DX + _DX * 0.5_F, j * _DY + _DY * 0.5_F};

//--------------------------------------------------------------------------------------------------------//
// obstable rectangulaire
//--------------------------------------------------------------------------------------------------------//
// cellules externes
/*if ( i==0 || i==_NX-1 || j==0 || j==_NY-1 || ( (fabs(xy[0]-1.0_F)<0.2_F) && (fabs(xy[1]-1.0_F)<0.1_F) ) ){
//if ( i==0 || i==_NX-1 || j==0 || j==_NY-1  ){
res=-1;
}
else{
  // bords externes
  if (i==_NX - 2) res+=1000*2; // east
  if (i==1) res+=100*1; // west
  if (j==_NY - 2) res+=10*2; // north
  if (j==1) res+=1*1; // south
  //obstacle interne carre cote 0.4x0.2 et centre en x=1,y=1
  if ( (fabs(xy[0]-(1.0_F-0.2_F))< _DX ) && (fabs(xy[1]-1.0_F)<0.1_F) ) res=1000; // cellule sur bord gauche de l'obstacle, CL paroi sur face east de la cellule
  if ( (fabs(xy[0]-(1.0_F+0.2_F))< _DX ) && (fabs(xy[1]-1.0_F)<0.1_F) ) res=100;  // cellule sur bord droit de l'obstacle, CL paroi sur face west de la cellule
  if ( (fabs(xy[1]-(1.0_F-0.1_F))< _DY ) && (fabs(xy[0]-1.0_F)<0.2_F) ) res=10; // cellule sur bord bas de l'obstacle, CL paroi sur face north de la cellule
  if ( (fabs(xy[1]-(1.0_F+0.1_F))< _DY ) && (fabs(xy[0]-1.0_F)<0.2_F) ) res=1; // cellule sur bord haut de l'obstacle, CL paroi sur face south de la cellule
}
*/
//--------------------------------------------------------------------------------------------------------//
// piece
//--------------------------------------------------------------------------------------------------------//
// cellules externes
if ( i==0 || i==_NX-1 || j==0 || j==_NY-1 || ( (fabs(xy[0]-5.0_F)>1.0_F) && (fabs(xy[1]-5.0_F)<0.2_F) ) ){
//if ( i==0 || i==_NX-1 || j==0 || j==_NY-1  ){
res=-1;
}
else{
  // bords externes
  if (i==_NX - 2) // east
    {
    if (xy[1]>5.0_F) res+=1000*2;
    else res+=1000*1;
    }
  if (i==1) // west
    {
    if (xy[1]>5.0_F) res+=100*2;
    else res+=100*1;
    }

  if (j==_NY - 2) res+=10*2; // north
  if (j==1) res+=1*1; // south
  //obstacle interne carre cote 4x2.0 et centre en x=0,y=15
  if ( (fabs(xy[0]-(0.0_F-4.0_F))< _DX ) && (fabs(xy[1]-5.0_F)<0.2_F) ) res=1000; // cellule sur bord gauche de l'obstacle, CL paroi sur face east de la cellule
  if ( (fabs(xy[0]-(0.0_F+4.0_F))< _DX ) && (fabs(xy[1]-5.0_F)<0.2_F) ) res=100;  // cellule sur bord droit de l'obstacle, CL paroi sur face west de la cellule
  if ( (fabs(xy[1]-(5.0_F-0.2_F))< _DY ) && (fabs(xy[0]-0.0_F)<4.0_F) ) res=10; // cellule sur bord bas de l'obstacle, CL paroi sur face north de la cellule
  if ( (fabs(xy[1]-(5.0_F+0.2_F))< _DY ) && (fabs(xy[0]-0.0_F)<4.0_F) ) res=1; // cellule sur bord haut de l'obstacle, CL paroi sur face south de la cellule
  //obstacle interne carre cote 4x0.2 et centre en x=0,y=15
  if ( (fabs(xy[0]-(10.0_F-4.0_F))< _DX ) && (fabs(xy[1]-5.0_F)<0.2_F) ) res=1000; // cellule sur bord gauche de l'obstacle, CL paroi sur face east de la cellule
  if ( (fabs(xy[0]-(10.0_F+4.0_F))< _DX ) && (fabs(xy[1]-5.0_F)<0.2_F) ) res=100;  // cellule sur bord droit de l'obstacle, CL paroi sur face west de la cellule
  if ( (fabs(xy[1]-(5.0_F-0.2_F))< _DY ) && (fabs(xy[0]-10.0_F)<4.0_F) ) res=10; // cellule sur bord bas de l'obstacle, CL paroi sur face north de la cellule
  if ( (fabs(xy[1]-(5.0_F+0.2_F))< _DY ) && (fabs(xy[0]-10.0_F)<4.0_F) ) res=1; // cellule sur bord haut de l'obstacle, CL paroi sur face south de la cellule
  // bords externes
  /*if (i==_NX - 2) res+=1000*2; // east
  if (i==1) res+=100*2; // west
  if (j==_NY - 2) res+=10*2; // north
  if (j==1) res+=1*2; // south*/
}

//--------------------------------------------------------------------------------------------------------//
// PBR parois
//--------------------------------------------------------------------------------------------------------//
// cellules externes
/*if ( i==0 || i==_NX-1 || j==0 || j==_NY-1 ){
res=-1;
}
else{
  // bords externes
  if (i==_NX - 2) res+=1000*1; // east
  if (i==1) res+=100*1; // west
  if (j==_NY - 2) res+=10*1; // north
  if (j==1) res+=1*1; // south
}
*/


return res;
}

///////////////////////////////////////////////////////////////////////
__kernel void init_sol(__global  float *wn){

  int id = get_global_id(0);
  
  int i = id % _NX;
  int j = id / _NX;

  int ngrid = _NX * _NY;

  float wnow[_M];

  float t = 0.0_F;
  float xy[2] = {i * _DX + _DX * 0.5_F, j * _DY + _DY * 0.5_F};
  
  exact_sol(xy, t, wnow);
  // load middle value
  for(int iv = 0; iv < _M; iv++){
    int imem = i + j * _NX + iv * ngrid;
    wn[imem] =  wnow[iv];
  }

}



///////////////////////////////////////////////////////////////////////
// premiere etape du RK2 pour MUSCL
__kernel void time_step_1(__global float *wn, __global float *wnp1,__global float *max_rayon_spec_cell){

  int id = get_global_id(0); // numero du proc
  
  int i = id % _NX;
  int j = id / _NX;

  int ngrid = _NX * _NY;
  
  float wnow[_M];
  float wnext[_M];
  //float maxrspec=-1.0_F;
  float dtloc=max_rayon_spec_cell[i + j * _NX]; // dt stocke de l'iteration precedente dans max_rayon_spec_cell

  int CL_cell=is_internal_cell(i,j);
  int filtre_CL[4]={1000,100,10,1};

  // load middle value
  for(int iv = 0; iv < _M; iv++){
    int imem = i + j * _NX + iv * ngrid;
    wnow[iv] = wn[imem]; // wn[imem] operation qui coute cher car lue en memoire globale (mais moins cher si memoire bien alignee),  wnow[iv] dans les registres du proc
    wnext[iv] = wnow[iv];
   }
  
  // only compute internal cells
  if (CL_cell>=0){
    float fluxP[_M]; // flux
    float rspecP; // rayon spectral
    float fluxM[_M]; // flux
    float rspecM; // rayon spectral
    // loop on directions x (east-west) and y (north-south)
    // idir = 0 (east) / idir = 1 (west) / idir = 2 (north) / idir = 3 (south)
    for(int idir = 0; idir < 4; idir+=2){
      float vnP[2];
      float vnM[2];
      // derivees pour MUSCL
      float dwM[_M];
      float dwnow[_M];
      float dwP[_M];
      // positionnement des normales
      vnP[0] = dir[idir][0];
      vnP[1] = dir[idir][1];
      vnM[0] = dir[idir+1][0];
      vnM[1] = dir[idir+1][1];
      // positionnement des voisins
      int iP = i + dir[idir][0];
      int jP = j + dir[idir][1];
      int iM = i + dir[idir+1][0];
      int jM = j + dir[idir+1][1];
      // positionnement des voisins des voisins P et M dans les direction x et y
      int ishift=idir/2; // permet d'aller chercher les voisins des voisins suivant east-west (ishift=0) et north-south (ishift = 1)
      int iPP = min(_NX-1,iP + (1-ishift));
      int jPP = min(_NY-1,jP + ishift);
      int iMM = max(0,iM - (1-ishift));
      int jMM = max(0,jM - ishift);
      // positionnement pas espace pour derivees
      float dx=ishift*_DY+(1-ishift)*_DX;
      // positionnement des CL
      int cl_idirP=CL_cell/filtre_CL[idir];
      CL_cell=CL_cell-filtre_CL[idir]*cl_idirP;
      int cl_idirM=CL_cell/filtre_CL[idir+1];
      CL_cell=CL_cell-filtre_CL[idir+1]*cl_idirM;
      // load neighbour values from global memory
      float wP[_M];
      float wM[_M];
      float wPP[_M];
      float wMM[_M];
      //-----------------------------------------
      // chargement etat P et M
      //-----------------------------------------
      for(int iv = 0; iv < _M; iv++){
	int imemP = iv * ngrid + iP + jP * _NX;
	wP[iv] = wn[imemP];
	int imemM = iv * ngrid + iM + jM * _NX;
	wM[iv] = wn[imemM];
      } 
      //-----------------------------------------
      // chargement etat PP et MM (voisins des voisins P et M dans direction x et y)
      //-----------------------------------------
      for(int iv = 0; iv < _M; iv++){
	int imemPP = iv * ngrid + iPP + jPP  * _NX;
	wPP[iv] = wn[imemPP];
	int imemMM = iv * ngrid + iMM + jMM * _NX;
	wMM[iv] = wn[imemMM];
      } 
      //-----------------------------------------
      // Conditions aux limites cellule P 
       //-----------------------------------------
     if (cl_idirP==1){ // CL paroi
	wP[0] = wnow[0];
	float qn = wnow[1] * vnP[0] + wnow[2] * vnP[1] ;
	wP[1] = wnow[1] - 2.0_F * qn * vnP[0] ;
	wP[2] = wnow[2] - 2.0_F * qn * vnP[1] ;
	wP[3] = wnow[3];
	wP[4] = wnow[4];
      }
      else if (cl_idirP==2){ // CL sortie
 	wP[0] = wnow[0];
	wP[1] = wnow[1];
	wP[2] = wnow[2];
	wP[3] = wnow[3];      
	wP[4] = wnow[4];
      } // fin if cl_idirP
      //-----------------------------------------
      // condition aux limites pour PP (ici recopie celles pour wP) ~ derivee nulle
      //-----------------------------------------
      if (cl_idirP>0){
        for(int iv = 0; iv < _M; iv++) wPP[iv] = wP[iv];
      }
      //-----------------------------------------
      // Conditions aux limites cellule M
      //-----------------------------------------
      if (cl_idirM==1){  // CL paroi
	wM[0] = wnow[0];
	float qn = wnow[1] * vnM[0] + wnow[2] * vnM[1] ;
	wM[1] = wnow[1] - 2.0_F * qn * vnM[0] ;
	wM[2] = wnow[2] - 2.0_F * qn * vnM[1] ;
	wM[3] = wnow[3];
	wM[4] = wnow[4];
      }
      else if (cl_idirM==2){ // CL sortie
 	wM[0] = wnow[0];
	wM[1] = wnow[1];
	wM[2] = wnow[2];
	wM[3] = wnow[3];      
	wM[4] = wnow[4];
      } // fin if cl_idirM
      //-----------------------------------------
      // condition aux limites pour MM (ici recopie celles pour wM) ~ derivee nulle
      //-----------------------------------------
      if (cl_idirM>0){
        for(int iv = 0; iv < _M; iv++) wMM[iv] = wM[iv];
      }
      //-----------------------------------------
      // calcul des derivees
      //-----------------------------------------
      // TODO: a optimiser pour eviter recalcul (2 fois) derivees dans arg minmod ? dx uniforme = pas util division par dx si multiplier apres ?
      for(int iv = 0; iv < _M; iv++){ 
        dwnow[iv]=minmod(wnow[iv]-wM[iv],wP[iv]-wnow[iv])/dx;
        dwM[iv]=minmod(wnow[iv]-wM[iv],wM[iv]-wMM[iv])/dx;
        dwP[iv]=minmod(wPP[iv]-wP[iv],wP[iv]-wnow[iv])/dx;
      }
      //-----------------------------------------
      // calcul des flux et bilan de mailles
      // ATTENTION les vecteurs wM, wMM, wP et wPP sont reutilises ici pour passer les arguments a fluxnum!
      //-----------------------------------------
      // calcul flux P
      for(int iv = 0; iv < _M; iv++){
        wPP[iv]=wP[iv]-0.5_F*dx*dwP[iv];
        wP[iv]=wnow[iv]+0.5_F*dx*dwnow[iv];
      }
      fluxnum(wP, wPP, vnP, fluxP, &rspecP);
      //maxrspec=fmax(rspecP,maxrspec); // ATTENTION : ici difference par rapport a time_step_2
      // calcul flux M
      for(int iv = 0; iv < _M; iv++){
        wMM[iv]=wM[iv]+0.5_F*dx*dwM[iv];
        wM[iv]=wnow[iv]-0.5_F*dx*dwnow[iv];
      }
      fluxnum(wM, wMM, vnM, fluxM, &rspecM);
      //maxrspec=fmax(rspecM,maxrspec); // ATTENTION : ici difference par rapport a time_step_2
      // time evolution / bilan maille
      // ATTENTION au plus devant fluxM ! c'est lie a vnM et fluxnum(UL=wnow, UR=wM, vnM, fluxM,&rspecM), cad etats L/R inverses.
      for(int iv = 0; iv < _M; iv++){
	wnext[iv] -= dtloc * ds[idir] / _VOL * (fluxP[iv] + fluxM[iv] ); 
      }   
      //-----------------------------------------
    }// fin idir
  }// end test for internal cells

  // stockage rayon spectrale pour calcul pas de temps dans le pyton 
  // ATTENTION : on ne doit pas modifier le pas de temps (stocker dans max_rayon_spec_cell) dans l'etape 1
  //             on a besoin du MEME pas de temps pour time_step 2, donc il est mis a jour dans la deuxieme etape du RK2 (time_step_2)
  //max_rayon_spec_cell[i + j * _NX]=maxrspec; // ATTENTION : ici difference par rapport a time_step_2

  // copy wnext into global memory
  // (including boundary cells)
  for(int iv = 0; iv < _M; iv++){
    int imem = iv * ngrid + i + j * _NX;
    wnp1[imem] = wnext[iv];
  }
 
} // fin time_step_1

///////////////////////////////////////////////////////////////////////
// deuxieme etape du RK2 pour MUSCL
__kernel void time_step_2(__global float *wn, __global float *wnp1,__global float *max_rayon_spec_cell){

  int id = get_global_id(0); // numero du proc
  
  int i = id % _NX;
  int j = id / _NX;

  int ngrid = _NX * _NY;
  
  float winit[_M];
  float wnow[_M];
  float wnext[_M];
  float maxrspec=-1.0_F;
  float dtloc=max_rayon_spec_cell[i + j * _NX]; // dt stocke de l'iteration precedente dans max_rayon_spec_cell

  int CL_cell=is_internal_cell(i,j);
  int filtre_CL[4]={1000,100,10,1};

  // load middle value
  for(int iv = 0; iv < _M; iv++){ // ATTENTION : ici difference par rapport a time_step_1
    int imem = i + j * _NX + iv * ngrid;
    winit[iv] = wn[imem]; 
    wnow[iv] = wnp1[imem]; 
    wnext[iv] = wnow[iv]; // necessaire pour cellules "fictives" a la fin routine
    }
  
  // only compute internal cells
  if (CL_cell>=0){
    float fluxP[_M]; // flux
    float rspecP; // rayon spectral
    float fluxM[_M]; // flux
    float rspecM; // rayon spectral
    // loop on directions x (east-west) and y (north-south)
    // idir = 0 (east) / idir = 1 (west) / idir = 2 (north) / idir = 3 (south)

    // mise a zero de wnext pour maille internes
    // important pour que le bilan ed mailles final soit le bon avec boucle sur idir
    for(int iv = 0; iv < _M; iv++){ // ATTENTION : ici difference par rapport a time_step_1
    wnext[iv] = 0.5_F*(winit[iv]+wnow[iv]) ; // necessaire pour cellules "fictives" a la fin routine
    }

    for(int idir = 0; idir < 4; idir+=2){
      float vnP[2];
      float vnM[2];
      // derivees pour MUSCL
      float dwM[_M];
      float dwnow[_M];
      float dwP[_M];
      // positionnement des normales
      vnP[0] = dir[idir][0];
      vnP[1] = dir[idir][1];
      vnM[0] = dir[idir+1][0];
      vnM[1] = dir[idir+1][1];
      // positionnement des voisins
      int iP = i + dir[idir][0];
      int jP = j + dir[idir][1];
      int iM = i + dir[idir+1][0];
      int jM = j + dir[idir+1][1];
      // positionnement des voisins des voisins P et M dans les direction x et y
      int ishift=idir/2; // permet d'aller chercher les voisins des voisins suivant east-west (ishift=0) et north-south (ishift = 1)
      int iPP = min(_NX-1,iP + (1-ishift));
      int jPP = min(_NY-1,jP + ishift);
      int iMM = max(0,iM - (1-ishift));
      int jMM = max(0,jM - ishift);
      // positionnement pas espace pour derivees
      float dx=ishift*_DY+(1-ishift)*_DX;
      // positionnement des CL
      int cl_idirP=CL_cell/filtre_CL[idir];
      CL_cell=CL_cell-filtre_CL[idir]*cl_idirP;
      int cl_idirM=CL_cell/filtre_CL[idir+1];
      CL_cell=CL_cell-filtre_CL[idir+1]*cl_idirM;
      // load neighbour values from global memory
      float wP[_M];
      float wM[_M];
      float wPP[_M];
      float wMM[_M];
      //-----------------------------------------
      // chargement etat P et M
      //-----------------------------------------
      for(int iv = 0; iv < _M; iv++){
	int imemP = iv * ngrid + iP + jP * _NX;
	wP[iv] = wnp1[imemP]; // ATTENTION : difference par rapport a time_step_1
	int imemM = iv * ngrid + iM + jM * _NX;
	wM[iv] = wnp1[imemM]; // ATTENTION : difference par rapport a time_step_1
      } 
      //-----------------------------------------
      // chargement etat PP et MM (voisins des voisins P et M dans direction x et y)
      //-----------------------------------------
      for(int iv = 0; iv < _M; iv++){
	int imemPP = iv * ngrid + iPP + jPP  * _NX;
	wPP[iv] = wnp1[imemPP]; // ATTENTION : difference par rapport a time_step_1
	int imemMM = iv * ngrid + iMM + jMM * _NX;
	wMM[iv] = wnp1[imemMM]; // ATTENTION : difference par rapport a time_step_1
      } 
      //-----------------------------------------
      // Conditions aux limites cellule P 
       //-----------------------------------------
     if (cl_idirP==1){ // CL paroi
	wP[0] = wnow[0];
	float qn = wnow[1] * vnP[0] + wnow[2] * vnP[1] ;
	wP[1] = wnow[1] - 2.0_F * qn * vnP[0] ;
	wP[2] = wnow[2] - 2.0_F * qn * vnP[1] ;
	wP[3] = wnow[3];
	wP[4] = wnow[4];
      }
      else if (cl_idirP==2){ // CL sortie
 	wP[0] = wnow[0];
	wP[1] = wnow[1];
	wP[2] = wnow[2];
	wP[3] = wnow[3];      
	wP[4] = wnow[4];
      } // fin if cl_idirP
      //-----------------------------------------
      // condition aux limites pour PP (ici recopie celles pour wP) ~ derivee nulle
      //-----------------------------------------
      if (cl_idirP>0){
        for(int iv = 0; iv < _M; iv++) wPP[iv] = wP[iv];
      }
      //-----------------------------------------
      // Conditions aux limites cellule M
      //-----------------------------------------
      if (cl_idirM==1){  // CL paroi
	wM[0] = wnow[0];
	float qn = wnow[1] * vnM[0] + wnow[2] * vnM[1] ;
	wM[1] = wnow[1] - 2.0_F * qn * vnM[0] ;
	wM[2] = wnow[2] - 2.0_F * qn * vnM[1] ;
	wM[3] = wnow[3];
	wM[4] = wnow[4];
      }
      else if (cl_idirM==2){ // CL sortie
 	wM[0] = wnow[0];
	wM[1] = wnow[1];
	wM[2] = wnow[2];
	wM[3] = wnow[3];      
	wM[4] = wnow[4];
      } // fin if cl_idirM
      //-----------------------------------------
      // condition aux limites pour MM (ici recopie celles pour wM) ~ derivee nulle
      //-----------------------------------------
      if (cl_idirM>0){
        for(int iv = 0; iv < _M; iv++) wMM[iv] = wM[iv];
      }
      //-----------------------------------------
      // calcul des derivees
      //-----------------------------------------
      for(int iv = 0; iv < _M; iv++){ // TODO: a optimiser pour eviter recalcul derivees dans arg minmod ? dx uniforme = pas util division par dx si multiplier apres ?
        dwnow[iv]=minmod(wnow[iv]-wM[iv],wP[iv]-wnow[iv])/dx;
        dwM[iv]=minmod(wnow[iv]-wM[iv],wM[iv]-wMM[iv])/dx;
        dwP[iv]=minmod(wPP[iv]-wP[iv],wP[iv]-wnow[iv])/dx;
      }
      //-----------------------------------------
      // calcul des flux et bilan de mailles
      // ATTENTION les vecteurs wM, wMM, wP et wPP sont reutilises ici pour passer les arguments a fluxnum!
      //-----------------------------------------
      // calcul flux P
      for(int iv = 0; iv < _M; iv++){
        wPP[iv]=wP[iv]-0.5_F*dx*dwP[iv];
        wP[iv]=wnow[iv]+0.5_F*dx*dwnow[iv];
      }
      fluxnum(wP, wPP, vnP, fluxP, &rspecP);
      maxrspec=fmax(rspecP,maxrspec); // ATTENTION : ici difference par rapport a time_step_1
      // calcul flux M
      for(int iv = 0; iv < _M; iv++){
        wMM[iv]=wM[iv]+0.5_F*dx*dwM[iv];
        wM[iv]=wnow[iv]-0.5_F*dx*dwnow[iv];
      }
      fluxnum(wM, wMM, vnM, fluxM, &rspecM);
      maxrspec=fmax(rspecM,maxrspec);// ATTENTION : ici difference par rapport a time_step_1
      // time evolution / bilan maille
      // ATTENTION au plus devant fluxM ! c'est lie a vnM et fluxnum(UL=wnow, UR=wM, vnM, fluxM,&rspecM), cad etats L/R inverses.
      for(int iv = 0; iv < _M; iv++){ 
	wnext[iv] -= 0.5_F * dtloc * ds[idir] / _VOL * (fluxP[iv] + fluxM[iv] ); // ATTENTION : ici difference par rapport a time_step_1
      }   
      //-----------------------------------------
    }// fin idir
  }// end test for internal cells

  // stockage rayon spectrale pour calcul pas de temps dans le pyton
  max_rayon_spec_cell[i + j * _NX]=maxrspec;// ATTENTION : ici difference par rapport a time_step_1

  // copy wnext into global memory
  // (including boundary cells)
  for(int iv = 0; iv < _M; iv++){
    int imem = iv * ngrid + i + j * _NX;
    wnp1[imem] = wnext[iv];
  }

} // fin time_step_2
