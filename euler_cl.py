#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""resolution of a transport equation by the finite volume method on regular grid"""


from __future__ import absolute_import, print_function
import pyopencl as cl
import numpy as np
import matplotlib.pyplot as plt
import time

# Load common utilities
import sys
sys.path.append('utils')
from utils import load_kernel, get_ite_title, parse_cl_args, save_to_png
from moduleh5py import *

## Default values

# number of conservative variables
_m = 5

# default grid size 
_nxy = 64
_nx = _nxy
_ny = _nxy
_Lx = 1.
_Ly = 1.


vmax = 2000.0 

# time stepping
cfl = 0.45
_Tmax = 0.0005

iteplot=800

def solve_ocl(m=_m, nx=_nx, ny=_ny, Tmax=_Tmax, Lx=_Lx, Ly=_Ly, animate=False, storehdf5=False, ivplot=0, precision="single", **kwargs):

    dx = Lx / nx
    dy = Ly / ny
    dt = cfl * min(dx,dy) / vmax # valeur initial dt sur base vmax

    # For plotting
    x = np.linspace(0., Lx, num=nx)
    y = np.linspace(0., Ly, num=ny)

    # load and adjust  C program
    parameters = {'nx': nx,
                  'ny': ny,
                  'dx': dx,
                  'dy': dy,
                  'dt': dt,
                  'm': m,
                  'lambda': vmax,
                  }

    np_real, source = load_kernel("euler_kernels.cl", parameters, precision=precision, print_source=False,
                                  module_file=__file__)
    
    # OpenCL init
    ctx = cl.create_some_context()
    mf = cl.mem_flags
    
    #affichage source vraiment compiler
    #print(source)

    # compile OpenCL C program
    prg = cl.Program(ctx, source).build(options="-cl-fast-relaxed-math -cl-single-precision-constant")

    dev = ctx.devices[0];
    
    log = prg.get_build_info(dev, cl.program_build_info.LOG).strip()
    print(log)
    
    # create OpenCL buffers
    buffer_size = m * nx * ny * np.dtype(np_real).itemsize
    wn_gpu = cl.Buffer(ctx, mf.READ_WRITE, size=buffer_size)
    wnp1_gpu = cl.Buffer(ctx, mf.READ_WRITE, size=buffer_size)

    buffer_size2 = nx * ny * np.dtype(np_real).itemsize
    max_rayon_spec_cell = cl.Buffer(ctx, mf.READ_WRITE, size=buffer_size2)

    # create a queue (for submitting opencl operations)
    queue = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)

    # init data
    event = prg.init_sol(queue, (nx * ny, ), (64, ), wn_gpu)
    event.wait()

    # ecriture entete fichier HDF5
    if storehdf5: 
      file_name='myfile.hdf5'
      save_hdf5_entete(file_name,1.4,720.0,nx,ny,Lx,Ly)

    # time loop
    t = 0
    ite = 0
    elapsed = 0.
    wn_cpu = np.empty((m * nx * ny, ), dtype=np_real)
    rspec_cpu = np.empty((nx * ny, ), dtype=np_real)

    # debut de la boucle en temps
    print("start OpenCL computations...")
    while t < Tmax:

        # pour calcul temps calcul hors post-traitement
        start = time.time()
        
        # MAJ valeurs de maille, etape 1 du RK2 pour MUSCL
        # (nx * ny, ) =nb cellules (1 cellule par proc) ;  (64, )=taille warp/work-group (peut etre laisser pour choix par opencl)
        event = prg.time_step_1(queue, (nx * ny, ), (64, ), wn_gpu, wnp1_gpu, max_rayon_spec_cell)
        event.wait()
        # MAJ valeurs de maille, etape 2 du RK2 pour MUSCL
        # (nx * ny, ) =nb cellules (1 cellule par proc) ;  (64, )=taille warp/work-group (peut etre laisser pour choix par opencl)
        event = prg.time_step_2(queue, (nx * ny, ), (64, ), wn_gpu, wnp1_gpu, max_rayon_spec_cell)
        event.wait()

        # calcul dt pour pas de temps suivant
        cl.enqueue_copy(queue, rspec_cpu, max_rayon_spec_cell).wait()
        vmaxloc=-1.0
        vmaxloc=np.amax(rspec_cpu)
        dt=cfl * min(dx,dy) / vmaxloc
        # on le stocke dans max_rayon_spec_cell
        rspec_cpu=np.full_like(rspec_cpu,dt)
        # on l'ecrit dans le buffer du GPU
        cl.enqueue_copy(queue, max_rayon_spec_cell, rspec_cpu).wait()

        # exchange buffer references for avoiding a copy
        wn_gpu, wnp1_gpu = wnp1_gpu, wn_gpu

        # calcul temps calcul hors post-traitement
        end = time.time()
        elapsed += 1.0* (end-start)

        # post-traitement/sauvegarde
        ite_title = get_ite_title(ite, t, dt*1.0e+6, elapsed)
        if ite % iteplot == 0 and storehdf5: # stockage dans fichier hdf5
          cl.enqueue_copy(queue, wn_cpu, wn_gpu).wait()
          save_hdf5_var(file_name,ite,t,wn_cpu,m,nx,ny)
        if ite % iteplot == 0 and animate: # stockage sous forme image png
          cl.enqueue_copy(queue, wn_cpu, wn_gpu).wait()
          save_to_png(wn_cpu,ite,t,m,nx,ny)
        else: # affichage info
          print(ite_title, end='\r') # affichage dynamique
          #print(ite,t,dt,vmaxloc) ## affichage classique/bourrin
        ite += 1
        t += dt
        # fin de la boucle en temps
   

    # copy OpenCL data to CPU and return the results
    cl.enqueue_copy(queue, wn_cpu, wn_gpu).wait()

    if storehdf5: # stockage dans fichier hdf5
      save_hdf5_var(file_name,ite,t,wn_cpu,m,nx,ny)

    #print(wn_cpu.nbytes)
    #print(wn_cpu.dtype)
    #print(wn_cpu[0])

    wplot_gpu = np.reshape(wn_cpu, (m, ny, nx))

    #lecture_info_hdf5(file_name)

    return x, y, wplot_gpu


if __name__ == '__main__':
    args = parse_cl_args(nx=_nxy, ny=_nxy, Lx=_Lx, Ly=_Ly, tmax=_Tmax, description='Solve Euler Perfect Gas equations using PyOpenCL')

    # gpu solve
    x, y, wplot_gpu = solve_ocl(**vars(args))

    wplot_gpu=np.flip(wplot_gpu,axis=1)

    if 1==0 : # sauvegarde brut fichier dernier pas de temps
      with open('data.txt','w') as f:
        for i in range(x.size):
          for j in range(y.size):
            rho=wplot_gpu[0, j,i]
            u=wplot_gpu[1, j,i]/wplot_gpu[0, j,i]
            v=wplot_gpu[2, j,i]/wplot_gpu[0, j,i]
            press=(wplot_gpu[3, j,i]-rho*(u*u+v*v)*0.5)*(1.4-1)
            txt=np.array([x[i],y[j],rho,u,v,press])
            txt.resize(1,6)
            np.savetxt(f, txt, fmt='%.8f')          
      f.close()

    if 1==0 : # sauvegarde dernier pas de temps pour PBR
      with open('data.txt','w') as f:
        for i in range(x.size):
          #j=1 # selon x ou y
          j=i # en biais
          rho=wplot_gpu[0, j,i]
          alpha=wplot_gpu[4, j,i]/rho
          u=wplot_gpu[1, j,i]/wplot_gpu[0, j,i]
          v=wplot_gpu[2, j,i]/wplot_gpu[0, j,i]
          press=(wplot_gpu[3, j,i]-rho*(u*u+v*v)*0.5)*(1.4-1)
          txt=np.array([(i+0.5)*1.0/x.size,rho,u,press,alpha])
          txt.resize(1,5)
          np.savetxt(f, txt, fmt='%.16f')          
      f.close()

     
    print("\n")
 
