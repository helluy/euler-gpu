"""A Figure class to plot animations or final plots"""

import argparse
from matplotlib import rcParams
import matplotlib.pyplot as plt
import numpy as np
import os

FF = "_F"  # string to be replaced in OpenCL kernel source

def float2str(real):
    """Return an exponential-formatted string from float"""
    return '{:e}{}'.format(real, FF)


def load_kernel(kernel_filename, parameters, precision="single", print_source=False, module_file='.'):
    """
    Args:
        kernel_filename: OpenCL kernel filename without full path
        parameters (dict): a dictionary of {variable name: value} for processing kernel source
        precision (str): single or double
        print_source (bool): print the actual kernel source
        module_file (str): required to run a main python script from outside its directory
    Returns:
        np_real (numpy type): numpy float according to precision
        source (str): kernel source using defined parameters dictionary

    """

    module_path = os.path.dirname(module_file)
    kernel_path = os.path.join(module_path, kernel_filename)
    print(module_file, kernel_path)
    with open(kernel_path, "r") as f:
        source = f.read()

    for k, v in parameters.items():
        val = float2str(v) if type(v) is not int else v
        source = source.replace('_{}_'.format(k), '({})'.format(val))

    if precision == "double":
        source = source.replace(FF, "")
        source = source.replace("float", "double")
        np_real = np.float64
    else:
        print("prec:", precision)
        source = source.replace(FF, "f")
        #source = source.replace("_real_", "float")
        np_real = np.float32

    if print_source:
        print(source)

    return np_real, source


def get_ite_title(ite, t, dt, elapsed):
    """Return a formatted string giving iteration information"""
    return "ite = {}, t = {:f}, dt (x10^6) = {:f}, elapsed (s) = {:f}".format(ite, t, dt, elapsed)


def parse_cl_args(nx=256, ny=256, Lx=1.0, Ly=1.0, tmax=1.0, description="Solve problem"):
    """
    Parse command line arguments to build a dictionary for solve_ocl() setting a square domain
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-s', '--storehdf5', action='store_true',
                        help='store results to hdf5 file')
    parser.add_argument('-a', '--animate', action='store_true',
                        help='real-time 2D plots')
    parser.add_argument('-sk', '--savekineticdata', action='store_true',
                        help='save kinetic data of simulation')
    parser.add_argument('-sm', '--savemacrodata', action='store_true',
                        help='save macro data of simulation')
    parser.add_argument('-nx', '--resolutionx', type=int,
                        default=nx, help='space resolution x')
    parser.add_argument('-ny', '--resolutiony', type=int,
                        default=ny, help='space resolution y')
    parser.add_argument('-Lx', '--lengthx', type=float,
                        default=Lx, help='Domain size')
    parser.add_argument('-Ly', '--lengthy', type=float,
                        default=Ly, help='Domain size')
    parser.add_argument('-t', '--Tmax', type=float, default=tmax,
                        help='final simulation time')
    parser.add_argument('-p', '--precision', type=str, default='single',
                        choices=['single', 'double'],
                        help='floating point precision')
    args = parser.parse_args()

    # Prepare to build a square domain with nx = ny = n
    arg_dict = vars(args)
    arg_dict['nx'] = arg_dict['resolutionx']
    del(arg_dict['resolutionx'])
    arg_dict['ny'] = arg_dict['resolutiony']
    del(arg_dict['resolutiony'])
    arg_dict['Lx'] = arg_dict['lengthx']
    del(arg_dict['lengthx'])
    arg_dict['Ly'] = arg_dict['lengthy']
    del(arg_dict['lengthy'])

    return args
    
def save_to_png(wn_cpu,ite=0,t=0,m=0,nx=0,ny=0):
    wplot = np.reshape(wn_cpu, (m, ny, nx))
    wplot = np.flip(wplot,axis=1)
    # figure
    fig = plt.figure(figsize=(7,7))
    # densite
    fig.add_subplot(2, 2, 1) # ligne, colonne, position
    plt.title("density, t = "+str(round(t,6))+" s")
    plt.imshow( wplot[0,:,:] , cmap = 'binary' , interpolation = 'nearest', vmin=0.5, vmax=9.0 )
    plt.colorbar()
    # pression
    fig.add_subplot(2, 2, 2) # ligne, colonne, position
    plt.title("pressure, t = "+str(round(t,6))+" s")
    press=0.4*(wplot[3,:,:]-0.5*(wplot[1,:,:]*wplot[1,:,:]+wplot[2,:,:]*wplot[2,:,:])/wplot[0,:,:])
    plt.imshow( press[:,:] , cmap = 'binary' , interpolation = 'nearest', vmin=5.0e4, vmax=8.0e5)
    plt.colorbar()
    #del press
    # norme vitesse
    fig.add_subplot(2, 2, 3) # ligne, colonne, position
    plt.title("velocity, t = "+str(round(t,6))+" s")
    vel=np.sqrt((wplot[1,:,:]*wplot[1,:,:]+wplot[2,:,:]*wplot[2,:,:])/wplot[0,:,:]/wplot[0,:,:])
    plt.imshow( vel[:,:] , cmap = 'binary' , interpolation = 'nearest', vmin=-50, vmax=4.0e2)
    plt.colorbar()
    #del vel
    # fraction
    fig.add_subplot(2, 2, 4) # ligne, colonne, position
    plt.title("fraction, t = "+str(round(t,6))+" s")
    plt.imshow( wplot[4,:,:]/wplot[0,:,:] , cmap = 'binary' , interpolation = 'nearest', vmin=0, vmax=1.0e0)
    plt.colorbar()
    # save figure en png
    fig.tight_layout()
    plt.savefig('data'+str(ite)+'.png')
    plt.close()
    del wplot
    del fig
    #plt.show()
    return

