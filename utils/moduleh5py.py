import h5py
import numpy as np

#-------------------------------------------------------------------------------
def save_hdf5_entete(file_name,gamma=1.4,Cv=720.0,nx=1,ny=1,Lx=1.0,Ly=1.0):
  # ouverture fichier
  f = h5py.File(file_name,'w')
  
  # groupe pour infos generales
  grp = f.create_group('infos')
  # -- EOS
  subgrp = f.create_group('infos/EOS_param')
  val=np.array(gamma)
  dset = subgrp.create_dataset("gamma", data=val)
  val=np.array(Cv)
  dset = subgrp.create_dataset("CV", data=val)
  # -- geom
  subgrp = f.create_group('infos/GEOM_param')
  val=np.array(nx)
  dset = subgrp.create_dataset("NX", data=val)
  val=np.array(ny)
  dset = subgrp.create_dataset("NY", data=val)
  val=np.array(Lx)
  dset = subgrp.create_dataset("LX", data=val)
  val=np.array(Ly)
  dset = subgrp.create_dataset("LY", data=val)

  # fermeture
  f.close()

#-------------------------------------------------------------------------------
def save_hdf5_var(file_name,ite,tps,var,nvar,nx,ny):
  # ouverture fichier
  f = h5py.File(file_name,'a')

  w = np.reshape(var, (nvar, ny, nx))
  w = np.flip(w,axis=1)

  # groupe pour iteration ite
  grp = f.create_group('iteration '+str(ite))
  # -- temps
  t=np.array(tps)
  dset = grp.create_dataset("time", data=t)
  # -- variables
  for iv in range(nvar):
    dset = grp.create_dataset("var "+str(iv), data=w[iv,:,:])
    #print("var "+str(iv))
    
  del w
  # fermeture
  f.close()
  
#----------------------------------------------------------------------

def lecture_info_hdf5(file_name,flag="short"):
  f = h5py.File(file_name,'r')
  liste_grp=f.keys() # liste des groupes
  
  for g in liste_grp:
    print(g)
    grp=f.get(g)
    liste_var=grp.keys() # liste des datasets, sauf pour groupe infos qui contient des sous groupes
    for v in liste_var:
      print('  '+v)
      if (flag=="long"):
        arr=np.array(grp.get(v)) # recuperation variable sous forme de numpy.array
        print('    ->',arr)
      
  f.close()
  
#----------------------------------------------------------------------

# post-traite uniquement l'iteration ite
def hdf5_to_grace(file_name,ite):
  f = h5py.File(file_name,'r')
  liste_grp=f.keys() # liste des groupes
  CV=0
  gamma=0
  LX=0
  LY=0
  NX=1
  NY=1
  temps=0.0

  for g in liste_grp:
    #print(g)
    grp=f.get(g)
    liste_var=grp.keys() # liste des datasets, sauf pour groupe infos qui contient des sous groupes
    for v in liste_var:
      #print('  '+v)
      if (g=='infos' and v=='EOS_param'):
        grp_eos=grp.get(v)
        CV=np.array(grp_eos.get("CV"))
        gamma=np.array(grp_eos.get('gamma'))
      if (g=='infos' and v=='GEOM_param'):
        grp_eos=grp.get(v)
        LX=np.array(grp_eos.get("LX"))
        LY=np.array(grp_eos.get("LY"))
        NX=np.array(grp_eos.get("NX"))
        NY=np.array(grp_eos.get("NY"))
      elif (g.split(" ")[0]=='iteration'):
        if (g.split(" ")[1]==str(ite) and v=='time'):
          temps=np.array(grp.get(v))
        elif (g.split(" ")[1]==str(ite) and v.split(" ")[0]=='var'):
          if (v.split(" ")[1]=='0'):
            rho=np.array(grp.get(v)) 
          elif (v.split(" ")[1]=='1'):
            rho_u=np.array(grp.get(v)) 
          elif (v.split(" ")[1]=='2'):
            rho_v=np.array(grp.get(v)) 
          elif (v.split(" ")[1]=='3'):
            rho_E=np.array(grp.get(v)) 
          elif (v.split(" ")[1]=='4'):
            rho_y=np.array(grp.get(v)) 
    # fin for v
  # fin for g          
  f.close()

# fin lecture fichier hdf5
            
  #print(rho[0,0],rho_u[0,0],rho_v[0,0],rho_E[0,0],rho_y[0,0])
  print("temps = ",temps)
  print("parametres = ",CV,gamma,LX,LY,NX,NY)

  with open('data_'+str(ite)+'_.txt','w') as f:
    for i in range(0,NX):
      j=1 # selon x ou y
      #j=i # en biais
      frac=rho_y[j,i]/rho[j,i]
      u=rho_u[j,i]/rho[j,i]
      v=rho_v[j,i]/rho[j,i]
      press=(rho_E[j,i]-rho[j,i]*(u*u+v*v)*0.5)*(1.4-1)
      txt=np.array([(i+0.5)*1.0/NX,frac,rho[j,i],u,press])
      txt.resize(1,5)
      np.savetxt(f, txt, fmt='%.16f')          
    f.close()

